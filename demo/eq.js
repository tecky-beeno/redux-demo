function add(x) {
    if (x === undefined) {
        x = 0;
    }
    return x + 1;
}
var initState = 5;
function lazyAdd(x) {
    if (x === void 0) { x = initState; }
    return x + 1;
}
console.log(add(2));
console.log(add());
