import {createStore} from "redux"

export type IRootState = {
    interval: number
    color1: string
    color2: string
    color3: string
}

function update(lastState: IRootState | undefined, action: any): IRootState {
    console.log('I am the function of createStore, I am called now. I am given some argument:', arguments)

    // when this function is called the first time, lastState will have nothing (be undefined)
    if (lastState === undefined) {
        return {
            interval: 20,
            color1: '#61DAFB',
            color2: '#61DAFB',
            color3: '#61DAFB',
        }
    }

    return {
        ...lastState,
        ...action
    }
}

let store = createStore(update)

// store.getState().interval
// store.dispatch({type: 'set color'})

export default store
