import React, {useState} from 'react';
import logo from './logo.svg';
import './App.css';
import Counter from './Counter'
import {Provider, useDispatch, useSelector} from "react-redux";
import store, {IRootState} from "./store";

function Page1() {
    let color = useSelector((state: IRootState) => state.color1)
    let dispatch = useDispatch()

    function setColor(value: string) {
        dispatch({type: 'set', color1: value})
    }

    return <div>
        <h1>Page 1</h1>
        <Counter
            color={color}
            setColor={setColor}/>
    </div>
}

function Page2() {
    let color = useSelector((state: IRootState) => state.color2)


    let dispatch = useDispatch()

    function setColor(value: string) {
        dispatch({type: 'set', color2: value})
    }


    return <div>
        <h1>Page 2</h1>
        <Counter
            color={color}
            setColor={setColor}/>
    </div>
}

function Page3() {
    let color = useSelector((state: IRootState) => state.color3)


    let dispatch = useDispatch()

    function setColor(value: string) {
        dispatch({type: 'set', color3: value})
    }


    return <div>
        <h1>Page 3</h1>
        <Counter
            color={color}
            setColor={setColor}/>
    </div>
}


function App() {
    const [interval, setInterval] = useState(20);
    const [page, setPage] = useState(1)

    return (
        <div className="App">
            <header className="App-header">
                <button onClick={() => setInterval(interval - 1)}>Speed Up</button>
                {interval}
                <button onClick={() => setInterval(interval + 1)}>Slow Down</button>

                <button onClick={() => setPage(1)}>Page 1</button>
                <button onClick={() => setPage(2)}>Page 2</button>
                <button onClick={() => setPage(3)}>Page 3</button>

                <div style={{border: '1px solid red'}}>
                    {function () {
                        switch (page) {
                            case 1:
                                return <Page1/>
                            case 2:
                                return <Page2/>
                            case 3:
                                return <Page3/>
                        }
                    }()}
                </div>
            </header>
        </div>
    );
}

export default () =>
    <Provider store={store}>
        <App/>
    </Provider>

